# VSCode Settigns

This are my settings to make 

* dotnet core 1.1 work on newer operation systems ( >= fc26)
* provide ability to run vscode with different sets of plugins and configs

## How-To

extract libdotnet into `~/media/cloud/Tools/libdotnet/` or change path accordingly

add this to .bashrc

```config
# User specific aliases and functions
alias vi='vim'

# Visual Studio Code Aliases
# Open Code always in new window
alias code='code -n'
# Under FC27 i need older libs for dotnet 1.x.x
alias dotnet='LD_LIBRARY_PATH=~/media/cloud/Tools/libdotnet/:$LD_LIBRARY_PATH dotnet'
alias code-dotnet='export LD_LIBRARY_PATH=~/media/cloud/Tools/libdotnet/:$LD_LIBRARY_PATH && code -n --user-data-dir ~/.vscode-dotnet --extensions-dir ~/.vscode-dotnet/extensions/'
# Use different vscode homes for different languages
alias code-react-ts='code -n --user-data-dir ~/.vscode-react-ts --extensions-dir ~/.vscode-react-ts/extensions/'
alias code-react-flow='code -n --user-data-dir ~/.vscode-react-flow --extensions-dir ~/.vscode-react-flow/extensions/'
```

now you can run code by callung

`code-react-flow`

You can run different instances simultaneously by switching into new user context

`sudo -su$(whoami)` or `su $(whoami)` and then same command as above